from distutils.core import setup

setup(
    name="ConfigMaster",
    version='1.0.6-2',
    description="Programmatic configuration library for Python 3.",
    author_email="eyesismine@gmail.com",
    packages=["configmaster"],
    license="MIT"
)
